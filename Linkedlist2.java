package collectionarray;
class LinkedList1Node {
    public int data;
    public LinkedList1Node next;
    
    public LinkedList1Node(int data) {
        this.data = data;
    }
}

 class LinkedList1 {
    public LinkedList1Node head;
    
    public void append(int data) {
        if (head == null) {
            head = new LinkedList1Node(data);
            return;
        }
        
        LinkedList1Node current = head;
        while (current.next != null) {
            current = current.next;
        }
        current.next = new LinkedList1Node(data);
    }
    
    public static LinkedList1 findCommonElements(LinkedList1 listOne, LinkedList1 listTwo) {
        HashSet<Integer> set = new HashSet<Integer>();
        LinkedList1 result = new LinkedList1();
        
        LinkedList1Node current = listOne.head;
        while (current != null) {
            set.add(current.data);
            current = current.next;
        }
        
        current = listTwo.head;
        while (current != null) {
            if (set.contains(current.data)) {
                result.append(current.data);
            }
            current = current.next;
        }
        
        return result;
    }
}
public class Linkedlist2{
	public static void main(String[] args) {
        // Test case 1
        LinkedList1 listOne = new LinkedList1();
        listOne.append(10);
        listOne.append(12);
        listOne.append(21);
        listOne.append(1);
        listOne.append(53);
        
        LinkedList1 listTwo = new LinkedList1();
        listTwo.append(11);
        listTwo.append(21);
        listTwo.append(25);
        listTwo.append(53);
        listTwo.append(47);
        
        LinkedList1 result = LinkedList1.findCommonElements(listOne, listTwo);
        LinkedList1Node current = result.head;
        while (current != null) {
            System.out.print(current.data + "->");
            current = current.next;
        }
        System.out.println("null");
        
        // Test case 2
        listOne = new LinkedList1();
        listOne.append(51);
        listOne.append(45);
        listOne.append(63);
        listOne.append(15);
        listOne.append(82);
        
        listTwo = new LinkedList1();
        listTwo.append(19);
        listTwo.append(63);
        listTwo.append(51);
        listTwo.append(87);
        listTwo.append(82);
        
        result = LinkedList1.findCommonElements(listOne, listTwo);
        current = result.head;
        while (current != null) {
            System.out.print(current.data + "->");
            current = current.next;
        }
        System.out.println("");
    }
}