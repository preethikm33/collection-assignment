package collectionarray;
class LinkedList1 {
	 
    public int data;
    public LinkedList1 next;

    public LinkedList1(int data) {
        this.data = data;
        this.next = null;
    }
}

 class LinkedList {
    private LinkedList1 head;

    public LinkedList() {
        this.head = null;
    }

    public LinkedList1 getHead() {
        return head;
    }

    public void setHead(LinkedList1 head) {
        this.head = head;
    }

    public void insert(int data) {
        LinkedList1 newNode = new LinkedList1(data);
        if (head == null) {
            head = newNode;
        } else {
            LinkedList1 currentNode = head;
            while (currentNode.next != null) {
                currentNode = currentNode.next;
            }
            currentNode.next = newNode;
        }
    }

    public void removeDuplicates() {
        if (head == null) {
            return;
        }
        LinkedList1 currentNode = head;
        while (currentNode != null) {
            LinkedList1 runnerNode = currentNode;
            while (runnerNode.next != null) {
                if (runnerNode.next.data == currentNode.data) {
                    runnerNode.next = runnerNode.next.next;
                } else {
                    runnerNode = runnerNode.next;
                }
            }
            currentNode = currentNode.next;
        }
    }

    public void printList() {
        LinkedList1 currentNode = head;
        while (currentNode != null) {
            System.out.print(currentNode.data + "->");
            currentNode = currentNode.next;
        }
        System.out.println("");
    }
}
 
 public class Linkedlistass {
	    public static void main(String[] args) {
	        LinkedList list1 = new LinkedList();
	        list1.insert(10);
	        list1.insert(15);
	        list1.insert(21);
	        list1.insert(15);
	        list1.insert(10);
	        System.out.println("Before removing duplicates:");
	        list1.printList();
	        list1.removeDuplicates();
	        System.out.println("After removing duplicates:");
	        list1.printList();

	        LinkedList list2 = new LinkedList();
	        list2.insert(51);
	        list2.insert(45);
	        list2.insert(45);
	        list2.insert(15);
	        list2.insert(82);
	        list2.insert(51);
	        list2.insert(10);
	        System.out.println("Before removing duplicates:");
	        list2.printList();
	        list2.removeDuplicates();
	        System.out.println("After removing duplicates:");
	        list2.printList();
	    }
	}

 