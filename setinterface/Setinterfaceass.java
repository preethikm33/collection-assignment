package collectionarray;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

 
	class Student1 {
		private int Student1Id;
		private String Student1Name;
		private String emailId;
		private String event;

		public Student1(int Student1Id, String Student1Name, String emailId, String event) {
			this.Student1Id = Student1Id;
			this.Student1Name = Student1Name;
			this.emailId = emailId;
			this.event = event;
		}

		public int getStudent1Id() {
			return Student1Id;
		}

		public void setStudent1Id(int Student1Id) {
			this.Student1Id = Student1Id;
		}

		public String getStudent1Name() {
			return Student1Name;
		}

		public void setStudent1Name(String Student1Name) {
			this.Student1Name = Student1Name;
		}

		public String getEmailId() {
			return emailId;
		}

		public void setEmailId(String emailId) {
			this.emailId = emailId;
		}

		public String getEvent() {
			return event;
		}

		public void setEvent(String event) {
			this.event = event;
		}

		@Override
		public boolean equals(Object Student1) {
			Student1 otherStudent1 = (Student1) Student1;
			if (this.emailId.equals(otherStudent1.emailId))
				return true;
			return false;
		}

		@Override
		public int hashCode() {
			return emailId.hashCode();
		}

		@Override
		public String toString() {
			return "Student1 Id: " + Student1Id + ", Student1 Name: " + Student1Name + ", Email Id: " + emailId;
		}
	}

	

public class Setinterfaceass {

    public static Set<Student1> findUnique(List<Student1> Student1s) {
        Set<Student1> uniqueStudent1s = new HashSet<Student1>();
        Set<String> emailIds = new HashSet<String>();
        for (Student1 Student1 : Student1s) {
            if (!emailIds.contains(Student1.getEmailId())) {
                uniqueStudent1s.add(Student1);
                emailIds.add(Student1.getEmailId());
            }
        }
        return uniqueStudent1s;
    }

    public static Set<Student1> findDuplicates(List<Student1> Student1s) {
        Set<Student1> duplicateStudent1s = new HashSet<Student1>();
        Set<String> emailIds = new HashSet<String>();
        for (Student1 Student1 : Student1s) {
            if (emailIds.contains(Student1.getEmailId())) {
                duplicateStudent1s.add(Student1);
            } else {
                emailIds.add(Student1.getEmailId());
            }
        }
        return duplicateStudent1s;
    }

    public static void main(String[] args) {
        List<Student1> Student1s = new ArrayList<Student1>();

        Student1s.add(new Student1(5004, "Wyatt", "Wyatt@example.com", "Dance"));
        Student1s.add(new Student1(5010, "Lucy", "Lucy@example.com", "Dance"));
        Student1s.add(new Student1(5550, "Aaron", "Aaron@example.com", "Dance"));
        Student1s.add(new Student1(5560, "Ruby", "Ruby@example.com", "Dance"));
        Student1s.add(new Student1(5013, "Clara", "Clara@example.com", "Music"));
        Student1s.add(new Student1(5015, "Sophie", "Sophie@example.com", "Music"));
        Student1s.add(new Student1(5011, "Ivan", "Ivan@example.com", "Music"));
        Student1s.add(new Student1(4556, "Ben", "Ben@example.com", "Music"));
        Student1s.add(new Student1(3656, "John", "John@example.com", "Music"));
        Student1s.add(new Student1(6543, "Sam", "Sam@example.com", "Music"));
        Student1s.add(new Student1(455, "Ben", "Ben@example.com", "Dance"));

        System.out.println("Unique Student1s:");
        Set<Student1> uniqueStudent1s = findUnique(Student1s);
        for (Student1 Student1 : uniqueStudent1s) {
            System.out.println(Student1);
        }

        System.out.println("\nDuplicate Student1s:");
        Set<Student1> duplicateStudent1s = findDuplicates(Student1s);
        for (Student1 Student1 : duplicateStudent1s) {
            System.out.println(Student1);
        }
    }

}
